# <img src="img/paddle.png" style="height:20px;"> Paddle
Paddle is a task manager based on the kanban methodology for bucketizing tasks.  Paddle is a single-page app that works in most modern browsers (including mobile).

![](img/screenshots/paddle_next.png)

<h2> <a href="https://lucidlylogicole.gitlab.io/paddle/paddle.html" target="_blank"> Launch Paddle</a>
&nbsp;&nbsp;&nbsp;&nbsp;
<!--<a href="https://lucidlylogicole.gitlab.io/paddle/demo.html" target="_blank">Demo</a>
</h2>-->

## Features
- Organize tasks by projects and category
- Bucket tasks in future, backlog, or next
- Data is saved in browser local storage
- Mobile friendly
- Optional Priority and Target Date
- Archive tasks and view reports

----
## Requirements
- A modern HTML5 Browser
- Browser local/indexed storage enabled (usually is by default)

### License
- [GPL 3](LICENSE.txt)

----
## Layout

### Next / Today / Week
These tabs show a summary of all tasks that are currently to do for their respective page. The tasks are organized by project and category.

![](img/screenshots/paddle_next.png)

### Projects
The Projects tab lists all of the projects.

**New Project** - The button to add a new project is available here.

![](img/screenshots/paddle_projects.png)

### Reports
The Reports tab will show all tasks that have been complete or cancelled (including archived). These are sorted by month, project, and category.

![](img/screenshots/paddle_reports.png)

----
## Organization
Tasks are organized into projects and categories. The categories allow for organizing the projects into more relevant sections of tasks.

- Project
    - Category
        - Tasks

![](img/screenshots/paddle_home_organize.png)

----
## Project Page
The project page view shows all *next* and *backlog* activities for the project.

![](img/screenshots/paddle_project.png)

### Goto Project
To view the whole project - click on the project title on the Home tab or on the Projects tab. The Projects tab lists all projects, not just the ones with current tasks.

### Project Menu
The project menu is located in the top right corner of the project box

![](img/screenshots/paddle_page_menu.png)

- Toggle tasks to view future or completed tasks (archived tasks are not visible here)
- Edit Categories - create, delete, or toggle the view of the categories
- Rename the project
- Delete the project

----
## Categories
Categories split up tasks for a project into sections

### New Category
1. Click the Panel Menu and choose *Edit Categories*
2. A panel will open listing the categories for the current project
3. Click the New Category button

### Toggle Category View
To toggle the visibility of a category (if not active), go to the Edit Categories panel (from project menu) and check/uncheck the box next to the category

### Delete Category
This is also done from the category panel. Click on the red x to remove the category and all tasks

### Rename Category
Click on the text of the category in the project.

----
## Tasks

### New Tasks
Click on the add button to the right of the Category title to add a task to that category

### Task Edit Panel
Editing the task information is done with the Task Edit Panel. Click on a task to open this panel. Click OK to save changes.

![](img/screenshots/paddle_task_edit.png)

- **Task Text** - the text that shows up in the main screen
- **Notes** - any details or additional notes for the task
- **Priority** - an optional number for prioritizing tasks. 1 being most important.
- **Bug Checkbox** - check if this is a bug
- **Target Date** - Optional field for tasks that have a specific target date
- **Created** - displays the date the task was created
- **Complete** - displays the date the task was marked done or cancelled
- **Delete** - button to permanently delete the task

### Change Task Status/Buckets
Tasks are placed in buckets.  By default, *future* and *done/completed* tasks are hidden.  Focus on the tasks in the *next* category. 

To change the status of the task, click on the icon left of the task. A menu of the available status will then popup.

![](img/screenshots/paddle_task_menu.png)

- <img src="img/future.png" style="height:14px;"> **Future** - a placeholder for ideas, but not recently planned.  These may or may not be moved to the backlog.
- <img src="img/backlog.png" style="height:14px;">  **Backlog** - tasks in queue.  These tasks will be moved to the next category, once the next tasks are complete.
- <img src="img/next.png" style="height:14px;">  **Next/Doing** - tasks currently being worked on (or will be worked on very soon)
- <img src="img/done.png" style="height:14px;"> **Done** - the task has been completed
- <img src="img/cancel.png" style="height:14px;"> **Cancel** - the task has been cancelled


----
## Menu Panel
Click the 3 bars icon on the top left of the screen to open the menu panel.

- **Saving** - Save the data to local storage with the browser. By default, the data is automatically saved unless that option is changed.
- **Import** - Import a paddle json file that was downloaded
- **Export** - Download the paddle data to a json file to be imported on another device or to save as backup.
- **Archive Tasks** - After a while the completed tasks will get very long, so these can be archived and will not show up on the main task screen.  These tasks can still be viewed through the report view.
- **Options**
    - **Autosave** - automatically save whenever there are changes to tasks or categories (This is checked true by default)

----
## Reference
Thanks to the following tools Paddle utilizes:
- Mustache - templating
- jQuery - nice JavaScript
- Dexie - indexedDB database