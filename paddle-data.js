
//---Database Setup

class PaddleDB {
    constructor() {
    	this.db = new Dexie('paddle');
    
    	// Define a schema
    	this.db.version(1).stores({
    		folder: '++id,name',  // Folders that projects are in
    		projects: '++id,name,fld_id,icon,active',
    		categories: '++id,name,proj_id,visible',
    		tasks:'++id,text,proj_id,cat_id,status,priority,create_date,target_date,plan_date,complete_date,notes',
    	});
    
    	// Open the database
    	this.db.open().catch(function(error) {
    		alert('Open Database Error: ' + error);
    	});
    }

    //---Project Functions
    getProjects() {
        return this.db.projects.orderBy('name').toArray()
    }
    getProject(proj_id) {
        return this.db.projects.get(proj_id)
    }
    addProject(projD) {
        return this.db.projects.add(projD)
    }
    addProjects(projD) {
        return this.db.projects.bulkAdd(projD)
    }
    removeProject(proj_id) {
        return this.db.tasks.where('proj_id')
            .equals(proj_id)
            .delete()
            .then(()=>{
                return this.db.categories.where('proj_id')
                    .equals(proj_id)
                    .delete()
                    .then(()=>{
                        return this.db.projects.delete(proj_id)
                    })
            })
    }

    updateProject(proj_id,projD) {
        return this.db.projects.update(proj_id,projD)
    }

    //---Category Functions
    getCategories(proj_id) {
        if (proj_id === undefined) {
            return this.db.categories.toArray()
        } else {
            return this.db.categories.where('proj_id').equals(proj_id).sortBy('name') //.toArray()
        }
    }
    // setCategoryVisible(cat_id,visible) {
        // return this.db.categories.update(cat_id,{visible:visible})
    // }
    addCategory(catgD) {
        return this.db.categories.add(catgD)
    }
    addCategories(catgD) {
        return this.db.categories.bulkAdd(catgD)
    }
    updateCategory(cat_id,catgD) {
        return this.db.categories.update(cat_id,catgD)
    }
    removeCategory(cat_id) {
        return this.db.categories.delete(cat_id)
    }
    
    //---Task Functions
    getTasks(proj_id,queryD) {
        if (proj_id === undefined) {
            return this.db.tasks.toArray()
        } else {
            return this.db.tasks.where('proj_id').equals(proj_id).toArray()
        }
        // console.log(queryD)
        // return this.db.tasks.where('proj_id')
        //     .equals(proj_id)
        //     .and(tsk => {
        //         return queryD.status.indexOf(tsk.status) > -1
        //     })
        //     .toArray()
    }
    getTaskInfo(task_id) {
        return this.db.tasks.get(task_id)
    }
    updateTask(task_id,taskD) {
        return this.db.tasks.update(task_id,taskD)
    }
    addTask(taskD) {
        return this.db.tasks.add(taskD)
    }
    addTasks(taskD) {
        return this.db.tasks.bulkAdd(taskD)
    }
    removeTask(task_id) {
        return this.db.tasks.delete(task_id)
    }
    
    //---Views
    getProjectTasks(queryD) {
        
        // QueryD in format with all keys optional:
        // {status:[], plan_start:date, plan_end:date, complete_start:date, complete_end:date}
        
        // Setup Status Query
        if (queryD.status === undefined) {
            queryD.status = 'next'
        }
        
        // let plan_end = (new Date('2100-01-01')).toISOString().split('T')[0]
        let plan_end = '2100-01-01'
        let plan_start = undefined
        
        // Setup Plan date query
        let plan_date_check = 0
        if (queryD.plan_start !== undefined) {
            // plan_start = queryD.plan_start.toISOString().split('T')[0]
            plan_start = queryD.plan_start
            plan_date_check = 1
            if (queryD.plan_end !== undefined) {
                // queryD.plan_end = new Date('2100-01-01')
                // plan_end = queryD.plan_end.toISOString().split('T')[0]
                plan_end = queryD.plan_end
            }
            
        }
        
        // Setup Complete date query
        let complete_date_check = 0
        if (queryD.complete_start !== undefined) {
            complete_date_check = 1
            if (queryD.complete_end === undefined) {
                queryD.complete_end = new Date('2100-01-01')
            }
        }

        // Project ID
        let proj_id = undefined
        if (queryD.proj_id !== undefined) {
            proj_id = queryD.proj_id
        }
        
        return this.db.tasks.where('status')
            .anyOf(queryD.status)
            .filter(tsk => {
                let ok_plan = 1
                let ok_compl = 1
                let ok_proj_id = 1
                if (plan_date_check) {
                    ok_plan =0 
                    if (tsk.plan_date !== undefined) {
                        // let plan_txt = tsk.plan_date.toISOString().split('T')[0]
                        // ok_plan =  plan_txt >= plan_start && plan_txt <= plan_end
                        ok_plan =  tsk.plan_date >= plan_start && tsk.plan_date <= plan_end
                    }
                }
                if (complete_date_check) {
                    ok_compl = 0
                    if (tsk.complete_date !== undefined) {
                        // let compl_txt = tsk.complete_date.toISOString().split('T')[0]
                        // ok_compl = compl_txt >= queryD.complete_start && compl_txt <= queryD.complete_end
                        ok_compl = tsk.complete_date >= queryD.complete_start && tsk.complete_date <= queryD.complete_end
                    }
                }
                if (proj_id !== undefined) {
                    ok_proj_id = tsk.proj_id == proj_id
                }
                // console.log(tsk.id,ok_plan && ok_compl && ok_proj_id)
                return ok_plan && ok_compl && ok_proj_id
            })
            .toArray()
    }
    
    // getTaskCounts() {
    //     let countD = {}
    //     db.tasks.where('status').anyOf(['next']).then(tsk => {
            
    //         if (countD[tsk.proj_id]== undefined) { countD[tsk.proj_id] = 0}
    //         countD[tsk.proj_id] += 1
                
    //     })
    // }
    
}

db = new PaddleDB()

function clearDatabase() {
    return Dexie.delete('paddle').then(()=>{
        db = new PaddleDB()
    })
    // return this.db.delete()
}
