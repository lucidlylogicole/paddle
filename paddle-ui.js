let version='2.0.5'
let statusA = ['future','backlog','next','done']
let monthNames = 'Jan,Feb,Mar,Apr,May,Jun,Jul,Aug,Sep,Oct,Nov,Dec'.split(',')
let statusView = ['backlog','next','idea']

function togglePage(page) {
    let pages = [
        'projects',
        'project',
        'report',
    ]
    for (let pg of pages) {
        if (pg != page) {
            $('#page-'+pg).hide()
        }
    }
    $('#page-'+page).show()
    
}

function highlightTab(page) {
    $('#toolbar span').removeClass('toolbar-active')
    $('#toolbar-'+page).addClass('toolbar-active')
    $('#nothing-todo').hide()
    $('#page-home').hide()
}

function hashChange(hash) {
    ok = 1
    let hash_split = location.hash.split('-')
    // console.log(hash_split)
    
    if (hash_split[0] == '#project') {
        if (hash_split[1] == 'all') {
            allProjectTasks()
        } else {
            viewProject(parseInt(hash_split[1]))
        }
    } else if (hash_split[0] =='#next') {
        nextView()
    } else if (hash_split[0] =='#today') {
        todayView()
    } else if (hash_split[0] =='#projects') {
        allProjects()
    } else if (hash_split[0] =='#report') {
        viewReport()
    } else {
        ok = 0
        $('#page-home').show()
    }
    
    return ok
    
}

//---Views
function allProjects() {
    db.getProjects().then(projs=>{
        let projsD = projs
        projsD.push({name:'All Projects',id:"'all'"})
        
        let countD = {}
        let status_list = ['next','backlog']
        // Get Tasks Counts
        db.getProjectTasks({status:status_list}).then(tasks=>{
            // Get Task Status Counts
            for (let t in tasks) {
                tsk = tasks[t]
                // console.log(tsk)
                if (countD[tsk.proj_id] === undefined){
                    countD[tsk.proj_id] = {}
                }
                if (countD[tsk.proj_id][tsk.status] === undefined){
                    countD[tsk.proj_id][tsk.status] = 0
                }
                countD[tsk.proj_id][tsk.status]+= 1
            }

            // Add Counts
            for (let p in projsD) {
                pD = projsD[p]
                // console.log(pD)
                if (pD.id in countD) {
                    for (ky in countD[pD.id]) {
                        pD[ky] = countD[pD.id][ky]
                    }
                }
            }
            // console.log(projsD)
            
            let h = Mustache.render($('#project-list-template').html(), {projects:projsD})
            $('#page-projects').html(h)
            togglePage('projects')
            location.hash='#projects'
        })
    })
    highlightTab('projects')
}

function nextView() {
    allProjectTasks({status:['next','idea']})
    location.hash='next'
    highlightTab('next')

}

function todayView() {
    let tdy = getDateString(new Date())
    allProjectTasks({status:['next','backlog','idea'],plan_start:'2018-03-01',plan_end:tdy})
    location.hash='#today'
    highlightTab('today')

}


function weekView(weekdate) {
    if (weekdate === undefined) {
        weekdate = new Date()
    }
    let week_dates =  getWeekDates(weekdate)
    // console.log(week_dates)
    allProjectTasks({status:['next','backlog','idea'],plan_start:getDateString(week_dates[0]),plan_end:getDateString(week_dates[1])})
    location.hash='#week'
    highlightTab('week')

}

function changeViewStatus(statusD) {
    for (let ky in statusD) {
        if (! (ky in statusView) && statusD[ky]) {
            statusView.push(ky)
        } else if (statusView.indexOf(ky) > -1 && ! statusD[ky]) {
            statusView.splice(statusView.indexOf(ky),1)
        }
    }
    hashChange(location.hash)
}


//---Project Page
function viewProject(proj_id) {
    togglePage('project')
    highlightTab()
    if (proj_id == 'all') {
        allProjectTasks()
    } else {
        $('#page-project').html('')
        getProjectContent(proj_id).then(proj_html=>{
            $('#page-project').append(proj_html)
        })
        location.hash='#project-'+proj_id
    }
}

function projectClick(e) {
    viewProject($(e.target).parent().data('proj-id'))
}

async function allProjectTasks(taskQuery) {
    
    togglePage('project')
    $('#page-project').html('')
    location.hash='#project-all'
    let projs = await db.getProjects()
    for (let proj in projs) {
        // console.log(proj,projs[proj])
        let proj_id = projs[proj].id
        await getProjectContent(proj_id,taskQuery).then(proj_html=>{
            $('#page-project').append(proj_html)
        
        })

    }
    
    if ($('#page-project').html().length == 0) {
        console.log('length3',$('#page-project').html().length)
        $('#nothing-todo').show()
    }
    
}

function getProjectContent(proj_id, taskQuery) {

    let project_view = 0 // project view shows all categories
    if (taskQuery === undefined) {
        project_view = 1
        taskQuery = {status:statusView}
    }
    taskQuery.proj_id = proj_id
    // console.log(taskQuery)

    return new Promise((resolve,reject)=>{
        let projectInfoD = {proj_id:proj_id,view_menu:project_view}
        db.getProject(proj_id).then(proj=>{
            // console.log(projs)
            
            if (proj !== undefined) {
                projectInfoD.proj_title = proj.name
                db.getCategories(proj_id).then(catgs => {
                    // Setup Categories
                    let catgD = {undefined:{cat_id:'',tasks:[],name:'',visible:1}}
                    let catg_sorted = [undefined] // keep track of sort order
                    for (let catg of catgs) {
                        catg_sorted.push(catg.id)
                        catgD[catg.id] = {cat_id:catg.id,tasks:[],name:catg.name,visible:catg.visible}
                    }
                    
                    // Setup Tasks 
                    db.getProjectTasks(taskQuery).then(taskD => {
                        
                        let used_categories = []
                        let task_count = 0
                        for (task of taskD) {
                            task_count += 1
                            let tempD = getTaskTemplateD(task)
                            if (catgD[task.cat_id] === undefined) {
                                task.cat_id = undefined
                            }
                            catgD[task.cat_id].tasks.push(tempD)
                            if (used_categories.indexOf(task.cat_id) ==-1) {
                                used_categories.push(task.cat_id)
                            }
                        }
                        
                        // Sort Categories
                        // catg_sorted.push(undefined)
                        catgA = []
                        for (let cid of catg_sorted) {
                            if (project_view || used_categories.indexOf(cid) > -1) {
                                catgA.push(catgD[cid])
                            }
                        }
                        // if (project_view) { catgA.push(catgD[undefined])}
                        
                        
                        // Build Template
                        projectInfoD.categories=catgA
                        // console.log(projectInfoD)
                        let proj_html = ''
                        if (task_count > 0 || project_view) {
                            proj_html = Mustache.render($('#project-template').html(), projectInfoD)
                        }
                        // $('#page-project').append(h)
                        resolve(proj_html)
                    })
                    
                })
            } else{
                reject('project id is undefined')
            }
            
        })
    })

}

function projectMenuShow(elm) {
    // console.log(elm)
    if ($('#project-menu').is(':visible')) {
        projectMenuClose()
    } else {
        $('#project-menu').data('proj-id',$(elm).parent().data('proj-id') )
        $('#project-menu').css( 'position', 'absolute' );
        let bottom = elm.position().top+elm.outerHeight(true)-2
        let left = elm.position().left + elm.outerWidth(true) -$('#project-menu').outerWidth(true)
        $('#project-menu').css( 'top', bottom );
        $('#project-menu').css( 'left', left);
        $('#project-menu').slideDown();
        elm[0].className = 'btn-project-menu active'
    }
}

function projectMenuClick(e) {
    let proj_id = parseInt($('#project-menu').data('proj-id'))
    let menu = $(e.target).data('menu')
    let menu_split = menu.split('_')
    
    // Close
    if (menu_split[0] != 'toggle') {
        let melm = $('.btn-project-menu')
        $('#project-menu').slideUp(function(){
            melm[0].className = 'btn-project-menu'
        })
    }
    
    // if (menu == 'new_category') {
        // newCategory(proj_id)
    if (menu == 'edit_category') {
        viewCategoryPanel(proj_id)
    } else if (menu == 'rename_project') {
        renameProject(proj_id)
    } else if (menu == 'delete_project') {
        deleteProject(proj_id)
    } else if (menu_split[0] == 'toggle') {
        // Toggle Status Views
        let stat = menu_split[1]
        let txt = 'Hide'
        console.log(stat)
        // taskFilter[stat] = !taskFilter[stat]
        let view = 1
        if (statusView.indexOf(stat)>-1) {
            view = 0
            txt = 'View'
        }
        
        changeViewStatus({[stat]:view})
        
        $('#project-menu .menu-item[data-menu=toggle_'+stat+']').find('.menu-view').html(txt)
        
    }
    
}

function projectMenuClose() {
    let page_id = $('#project-menu').attr('page-id')
    let elm = $('.btn-project-menu')
    $('#project-menu').slideUp(function(){
        elm[0].className = 'btn-project-menu'
    })
    
}

function newProject() {
    title = prompt('Enter Project Title')
    
    if (title != undefined) {
        db.addProject({name:title}).then(()=>{
            allProjects()
        })
    }
}

function deleteProject(proj_id) {
    ok = confirm('Permanently DELETE project and all associated tasks?')
    if (proj_id != undefined && ok) {
        db.removeProject(proj_id).then(()=>{
            allProjects()
        })
    }
}

function renameProject(proj_id) {
    ntxt = prompt('Edit Project Name',$('#project-page-'+proj_id+' .project-title-text').html())
    if (ntxt != undefined) {
        
        db.updateProject(proj_id,{name:ntxt}).then(()=>{
            $('#project-page-'+proj_id+' .project-title-text').html(ntxt)
        })
        
    }
}

//---Tasks
function taskClick(e) {
    let elm = $(e.target)
    let tid
    if (elm.hasClass('task-status')) {
        taskStatusDialog(elm,elm.parent().data('id'))
    }
    else if (elm.hasClass('task-text')) {
        tid = elm.parent().data('id')
    } else if (elm.hasClass('task-card')) {
        tid = elm.data('id')
    }
    if (tid) {
        editTask(tid)
        $('#status-dlg').hide()
    }
}

function newTask(proj_id,cat_id) {
    
    let tD = {
        proj_id:proj_id,
        cat_id:cat_id,
        text:'',
        status:'backlog',
        create_date:new Date(),
    }
    
    db.addTask(tD).then(task_id => {
        let htask = Mustache.render($('#task-template').html(), tD)
        if (cat_id === undefined) {cat_id = ''}
        // console.log('#catg-'+cat_id)
        $('#catg-'+cat_id+' .task-list').append(htask)
        editTask(task_id)
    })

}

function taskStatusDialog(elm, tid) {
    if ($('#status-dlg').is(':visible')) {
        $('#status-dlg').slideUp();
    } else {
        $('#status-dlg').attr('task-id',tid);
        $('#status-dlg').css( 'position', 'absolute' );
        let bottom = elm.parent().position().top+elm.parent().outerHeight(true)-1
        $('#status-dlg').css( 'top', bottom );
        $('#status-dlg').css( 'left', elm.parent().position().left);
        $('#status-dlg').slideDown();
        // $('#task-panel').hide()
    }
}

function editTaskStatus(e) {
    let elm = $(e.target)
    if (elm.is('img')) {
        elm = elm.parent()
    }
    let tid = parseInt(elm.parent().attr('task-id'))
    let stat = elm.attr('data-status')
    
    if (stat != 'close') {
        updD = {status:stat}
        if (stat == 'done' || stat=='cancel') {
            updD.complete_date = new Date()
        }
        
        db.updateTask(tid,updD).then(()=>{
            // Change Image
            let telm = $('#task-'+tid+' .task-status')
            telm[0].className = 'tblc task-status '+stat
            $('#task-'+tid).attr('class','task-card '+stat) //+ ' '+docD.tasks[tid].priority)
        })

    }
    
    $('#status-dlg').hide()
}

//---Task Edit Panel
function getTaskTemplateD(task) {
    let tempD = {
        text:task.text,
        id:task.id,
        status:task.status,
        card_status:task.status,
        priority_text:'',
    }
    
    if (task.priority != undefined && task.priority != '') {
        task.priority_text = task.priority
    }
    
    // Priority
    if (['bug','important','norush','idea'].indexOf(task.priority) > -1) {
        tempD.priority = task.priority
    } else if (task.priority != undefined && task.priority != '') {
        tempD.priority_text = task.priority
    }
    
    if (task.priority == 'norush' && task.status == 'next') {
        tempD.extra_status = 'norush'
    } else if (task.priority == 'bug') {
        tempD.extra_status = 'bug'
    } else if (task.priority == 'important') {
        tempD.extra_status = 'important'
    // } else if (task.priority == 'idea') {
        // tempD.extra_status = 'idea'
    }
    
    
    // Datez
    let targdate = new Date(task.target_date)
    if (!isNaN(targdate)) {
        let m = monthNames[targdate.getMonth()]
        tempD.target_date = m+' '+(targdate.getDate()+1)
    }

    return tempD
    
}

function editTask(task_id) {
    
    db.getTaskInfo(task_id).then(tD => {

        // console.log(tD)
    
        let cdate = ''
        let cmpdate = ''

        // Create Date
        if (tD.create_date) {
            let cdt = new Date(tD.create_date)
            cdate = cdt.toUTCString().substring(5,16)
        }
        
        // Completed Date
        if (tD.complete_date) {
            let cmpdt = new Date(tD.complete_date)
            cmpdate = cmpdt.toUTCString().substring(5,16)
        }
        
        $('#task-edit-title').val(tD.text)
        $('#task-edit-notes').val(tD.notes)
        $('#task-edit-id').html(tD.id)
        
        // let plan_date = ''
        // try {
        //     plan_date = tD.plan_date.toISOString().split('T')[0]
        // } catch(e){}
        
        // let target_date = ''
        // try {
        //     target_date = tD.target_date.toISOString().split('T')[0]
        // } catch(e){}
        
        $('#task-edit-plan-date').val(tD.plan_date)
        $('#task-edit-target-date').val(tD.target_date)
        $('#task-created').html(cdate)
        $('#task-complete').html(cmpdate)
        
        $('#task-edit-notes').parent().data('task-id',task_id)
        
        // Status
        task_edit_status_click(tD.status)
        
        // Priority
        $('#task-edit-priority').val('')
        if (['bug','important','norush','idea'].indexOf(tD.priority) > -1) {
            $('#task-edit-'+tD.priority).prop("checked", true)
        } else {
            $('#task-edit-priority-val').prop("checked", true)
            
            $('#task-edit-priority').val(tD.priority)
        }
        
        $('#task-panel').animate({width:'show'},350);
        
        // Focus if not mobile
        if (! (navigator.userAgent.match(/Android/i) || navigator.userAgent.match(/iPhone/i))) {
            $('#task-edit-title').focus()
        }


    })
    
}

function editTaskSave() {
    let task_id = $('#task-edit-title').parent().data('task-id')
    let elm = $('#task-'+task_id)
    
    if (task_id != '') {
        let tD = {}
        tD.text = $('#task-edit-title').val()
        tD.notes = $('#task-edit-notes').val()
        tD.priority = $('#task-edit-priority').val()
        tD.plan_date = $('#task-edit-plan-date').val()
        tD.target_date = $('#task-edit-target-date').val()
        // tD.plan_date = new Date($('#task-edit-plan-date').val())
        // tD.target_date = new Date($('#task-edit-target-date').val())
        
        // Priority/Bug
        elm.find('.task-priority').html('')
        if ($('#task-edit-important').is(':checked')) {
            tD.priority = 'important'
            elm.find('.task-priority').addClass('important')
            elm.find('.task-priority').removeClass('bug')
            elm.find('.task-priority').removeClass('idea')
            // elm.find('.task-priority').removeClass('norush')
        }
        else if ($('#task-edit-bug').is(':checked')) {
            tD.priority = 'bug'
            elm.find('.task-priority').removeClass('important')
            elm.find('.task-priority').removeClass('idea')
            elm.find('.task-priority').addClass('bug')
        } else if ($('#task-edit-norush').is(':checked')) {
            tD.priority = 'norush'
            elm.find('.task-priority').removeClass('important')
            elm.find('.task-priority').removeClass('bug')
            elm.find('.task-priority').removeClass('idea')
        //     elm.find('.task-card').addClass('norush')
        } else if ($('#task-edit-idea').is(':checked')) {
            tD.priority = 'idea'
            elm.find('.task-priority').removeClass('important')
            elm.find('.task-priority').removeClass('bug')
            elm.find('.task-priority').addClass('idea')
        } else {
            elm.find('.task-priority').removeClass('bug')
            elm.find('.task-priority').removeClass('idea')
            elm.find('.task-priority').removeClass('important')
            // elm.find('.task-priority').removeClass('norush')
            elm.find('.task-priority').html(tD.priority)

        }

        // Status
        let stat_elm = $('.task-edit-status-select').attr('id')
        if (stat_elm) {
            let stat_id = stat_elm.split('-')
            let stat = stat_id[stat_id.length-1]
            tD.status = stat
            if (stat == 'done' || stat=='cancel') {
                tD.complete_date = new Date()
            }
            // Change Status Image
            let class_stat = stat
            if ($('#task-edit-norush').is(':checked')) {
                class_stat += ' norush'
            }
            let telm = $('#task-'+task_id+' .task-status')
            telm[0].className = 'tblc task-status '+class_stat
            // let pelm = $('#task-'+task_id)
            $('#task-'+task_id).attr('class','task-card '+class_stat+ ' '+tD.priority)
        }
        
        // Target Date        
        let targdate = new Date(tD.target_date)
        if (isNaN(targdate)) {
            elm.find('.task-target-date').html('')
        }
        else {
            targdate = new Date(tD.target_date)
            let m = monthNames[targdate.getMonth()]
            elm.find('.task-target-date').html(m+' '+(targdate.getDate()+1))
        }
        
        // Save
        db.updateTask(task_id,tD).then(()=>{
            $('#task-'+task_id+' .task-text').html(tD.text)
            closeTaskPanel()
        })
                    
    }
    $('#task-edit-title').parent().data('task-id','')
    
}

function closeTaskPanel() {
    $('#task-panel').animate({width:'toggle'},350);
}
function task_edit_status_click(status) {
    $('#task-edit-status span').removeClass()
    $('#task-edit-status-'+status).addClass('task-edit-status-select')
}

function snooze(days) {
    // plan_date = new Date($('#task-edit-plan-date').val())
    // if (isNaN(plan_date)) {
    //     plan_date = new Date()
    // }
    
    let plan_date = stringToDate($('#task-edit-plan-date').val())
    let tdy = new Date()
    if (plan_date < tdy) {
        plan_date = tdy
    }
    
    plan_date.setDate(plan_date.getDate() + days)
    // plan_date.setDate(plan_date.getTime()+1000*60*60*24*days)
    
    // new_date = new Date(plan_date.getFullYear()-2,plan_date.getMonth()+22,plan_date.getDate()+60+days)
    // new_date = new Date(plan_date.getFullYear(),plan_date.getMonth(),plan_date.getDate()+days)
    
    $('#task-edit-plan-date').val(getDateString(plan_date))

}

function planToday() {
    plan_date = new Date()
    $('#task-edit-plan-date').val(getDateString(plan_date))
}

function deleteTask(task_id) {
    if (task_id === undefined) {
        ok = confirm('Do you really want to delete the task?')
        if (ok) {
            task_id = $('#task-edit-title').parent().data('task-id')
        }
    }
    if (task_id != '' && task_id != undefined) {
        task_id = parseInt(task_id)
        
        // Delete Task
        db.removeTask(task_id)
        
        $('#task-'+task_id).remove()
        $('#task-edit-title').parent().data('task-id','')
        $('#task-panel').hide()
        
    }
}

//---Category Panel
function viewCategoryPanel(proj_id) {
    
    db.getCategories(proj_id).then(catgs => {
        
        $('#category-panel').data('proj-id',proj_id)
        
        // Setup Categories
        let h = ''
        let catgD = {undefined:{id:'',tasks:[],name:'misc'}}
        for (let catg of catgs) {
        
            h += '<div class="panel-catg"><input type=checkbox'
            if (catg.visible) {
                h += ' checked'
            }
            h += ' onclick="toggleCategoryVisible('+catg.id+')"> '
            h += catg.name
            h += '<span onclick="deleteCategory('+catg.id+')" class="panel-catg-delete" title="delete"><img src="img/delete.png"></span>'
            h += '</div>'
        }
        
        $('#category_list').html(h)
        if (!$('#category-panel').is(':visible')) {
            $('#category-panel').animate({width:'toggle'},350);
        }
        
    })
    
}

function newCategory(title) {
    
    let proj_id = $('#category-panel').data('proj-id')
    
    if (title === undefined) {
        title = prompt('Category Title')
    }
    
    if (title !== undefined && proj_id !== undefined) {
        catD = {
            name:title,
            visible:1,
            proj_id:proj_id,
        }
        
        db.addCategory(catD).then(()=>{
            viewProject(proj_id)
            viewCategoryPanel(proj_id)
        })
    }
}

function editCategory(e) {
    elm = $(e.target)
    cid = elm.parent().data('id')  // row

    if (cid != undefined) {
        ntxt = prompt('Edit Category Name',elm.html())
        if (ntxt != undefined) {
            
            db.updateCategory(cid,{name:ntxt}).then( ()=>{
                elm.html(ntxt)
            })

        }
    }
}

function toggleCategoryVisible(cat_id) {
    
    db.updateCategory(cat_id,{visible:!$('#catg-'+cat_id).is(':visible')}).then( ()=>{
        if ($('#catg-'+cat_id).is(':visible')) {
            $('#catg-'+cat_id).hide();
        }
        else {
            $('#catg-'+cat_id).show();
        }
    })
}

function deleteCategory(cat_id) {
    cat_id = parseInt(cat_id)
    ok = confirm('Do you want to delete the category?')
    if (ok) {
        
        db.removeCategory(cat_id).then(()=>{
            $('#catg-'+cat_id).remove()
            $('#catg-'+cat_id).remove()
            viewCategoryPanel($('#category-panel').data('proj-id'))
        })
        
    }
}

//---Import/Export
function import_tasks(event) {
    let files = event.target.files
    let reader = new FileReader()
    
    // parse the file - reader is async
    reader.onload = function(theFile) {
        
        ok = confirm('Clear the database and import the file?')
        
        if (ok) {
        
            clearDatabase().then(()=>{
                
                try {
                    fstr = theFile.target.result
                    let importD = JSON.parse(fstr)
                    // console.log(importD)
                    
                    // Projects
                    db.addProjects(importD.projects).then(()=>{
                        // Categories
                        db.addCategories(importD.categories).then(()=>{
                            // Tasks
                            db.addTasks(importD.tasks).then(()=>{
                                if (! hashChange()) {
                                    nextView()
                                }
                                alert('Import complete!')
                            })
                        })
                    })
                    
                    
                    // // Load Projects
                    // for (let pD of importD.projects) {
                    //   db.addProject(pD)
                    
                    // }
                    
                    // // Load Cateogires
                    // for (let cD of importD.categories) {
                    //     db.addCategory(cD)
                    // }
                    
                    // // Load Tasks
                    // for (let tD of importD.tasks) {
                    //     db.addTask(tD)
                    // }
                    
                    
                } catch (e) {
                    alert('There was an error importing the file')
                }
            })
        }
    }
    // reader.onerror = function(){er=1;console.log('Error reading file')}       // error message

    reader.readAsText(files[0])
    $('#import_section').hide()
    $('#datafile').val('')
    
}

async function toJson() {
    // Return json of paddle database
    let exportD = {
        projects:[],
        categories:[],
        tasks:[],
    }
    
    // Projects
    let projs = await db.getProjects()
    exportD.projects = projs
    
    // Categories
    let catgs = await db.getCategories()
    exportD.categories = catgs

    // Tasks
    let tasks = await db.getTasks()
    exportD.tasks = tasks

    return exportD
}

async function exportPaddle() {
    filename = prompt('Download Filename','Paddle_tasks.json')
    
    // Template


    if (filename) {
        
        let exportD = await toJson()
        
        // Export to file
        let data = "data:text/json;charset=utf-8," + encodeURIComponent(JSON.stringify(exportD));
        let dl_elm = document.getElementById('download_element');
        dl_elm.setAttribute("href",data);
        dl_elm.setAttribute("download", filename);
        dl_elm.click()
    }
    
}

//---
//---Reports
async function viewReport() {
    togglePage('report')
    highlightTab('report')
    location.hash='#report'

    // Projects
    let projs = await db.getProjects()
    let projD = {}
    for (let p in projs) {
        let pD = projs[p]
        projD[pD.id] = pD.name
    }
    
    //---todo:load categories
    
    // Tasks
    let taskQuery = {status:['done','cancelled']}
    reportD = {}
    
    // Get Completed Tasks
    db.getProjectTasks(taskQuery).then(tasks =>{
        
        for (let t in tasks) {
            tD = tasks[t]
            if (tD.complete_date !== undefined) {
                let dts = getDateString(new Date(tD.complete_date)).substr(0,7)
                if (! (dts in reportD)) {reportD[dts]={}}
                if (!(tD.proj_id in reportD[dts])) {reportD[dts][tD.proj_id]=[]}
                reportD[dts][tD.proj_id].push({id:tD.id,text:tD.text,status:tD.status})
            }
        }
        
        // Build Report
        let tempD = {'dates':[]} // template
        let monthyears = Object.keys(reportD).sort().reverse()
        for (let my in monthyears) {
            let dt = monthyears[my]
            let dt_splt = dt.split('-')
            let dt_title = monthNames[parseInt(dt_splt[1])-1]+' '+dt_splt[0]
            let dtD = {title:dt_title,projects:[]}
            // tempD[dt] = []
            let myD = reportD[dt]
            for (var proj_id in myD) {
                let pD = {name:projD[proj_id],tasks:[]}
                for (var tid in myD[proj_id]) {
                    pD.tasks.push(myD[proj_id][tid])
                }
                dtD.projects.push(pD)
            }
            tempD.dates.push(dtD)
            
        }
        // console.log(tempD)
        
        let h = Mustache.render($('#report-template').html(), tempD)
        $('#page-report').html(h)
        
    })
    
}


//---
//---Date Functions
function getDateString(date) {
    let day = date.getDate()
    if (day < 10) {day = '0'+day}
    let mon = date.getMonth()+1
    if (mon < 10) {mon = '0'+mon}
    return ''+date.getFullYear()+'-'+mon+'-'+day
}

function stringToDate(date_string) {
    date = new Date()
    if (date_string != '') {
        let date_split = date_string.split('-')
        date = new Date(parseInt(date_split[0]),parseInt(date_split[1])-1,parseInt(date_split[2]))
    }
    return date
}

function getWeekDates(fromdate) {
    let first =  -fromdate.getDay()+1; // First day is the day of the month - the day of the week
    let last = first + 6; // last day is the first day + 7
    // console.log(fromdate,first,last)
    // let last = new Date(first.getDate() + 7); // last day is the first day + 7
    
    // let firstday = new Date(fromdate+first)
    // let lastday = new Date(fromdate+last)
    let firstday = new Date()
    firstday.setDate(fromdate.getDate() + first)
    let lastday = new Date()
    lastday.setDate(fromdate.getDate() + last)
    return [firstday,lastday]
}

function reloadAppCache() {
    var appCache = window.applicationCache;
    if (appCache.status == 1) {
        appCache.update(); // Attempt to update the user's cache.
    }
    location.reload(true)      // Reload the page
}


//---
//---Document Ready
$(document).ready(function() {
    //---Events
    $('#page-project').on('click','.task-card',taskClick)
    $('#project-menu .menu-item').on('click', projectMenuClick)
    $('#age-project').on('click','.task-card',taskClick)
    $('.status-sel').on('click', editTaskStatus)
    $('#page-project').on('click', '.catg-title', editCategory)
    $('#page-project').on('click','.project-title-text',projectClick)
    
    // Import
    document.getElementById('datafile').addEventListener('change', import_tasks, false)
    
    // window.onhashchange = hashChange
    
    if (! hashChange()) {
        // nextView()
        $('#page-home').show()
    }
    
    $('#version').html(version)
    
})